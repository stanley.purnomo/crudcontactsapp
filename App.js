/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { useEffect, useRef, useState } from 'react';
import { createStore } from 'redux';  // redux library
import 'whatwg-fetch' // for mocking jest only
// import type {PropsWithChildren} from 'react';
import {
  ActivityIndicator,
  Alert,
  Button,
  Dimensions,
  Image,
  Modal,
  Platform,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEdit, faEye, faPlus, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';

// type SectionProps = PropsWithChildren<{
//   title: string;
// }>;

let contactList = [];

async function crudReducer(state = { data: [] }, action) {
  switch (action.type) {
    case 'create':
      return { data: [...state.data, action.data] }
    case 'update':
      state.data[action.index] = action.data
      return { data: [...state.data]}
    case 'delete':
      state.data.splice(action.index, 1);
      return { data: [...state.data]}
    // case 'counter/decremented':
    //   return { value: state.value - 1 }
    default:
      return state
  }
}

let crud = createStore(crudReducer)

function Section({children, title}) {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
}

crud.subscribe(() => {
  console.log(crud.getState().data)
})

function App(){
  const isDarkMode = useColorScheme() === 'dark';

  const [valueState, setValueState] = useState(0)
  const [formState, setFormState] = useState('create')
  const [id, setID] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [age, setAge] = useState('')
  const [indexState, setIndexState] = useState(-1)
  const [modalVisible, setModalVisible] = useState(false)
  const [indicator, setIndicator] = useState(false)
  const [refresh, setRefresh] = useState(false)
  const [contacts, setContacts] = useState([])

  const updateContacts = () => { 
    console.log(JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      age: age,
      photo: 'N/A'
    }));
    if(firstName.length === 0 || lastName.length === 0 || age.length === 0){
      alert('Please fill all required fields')
    } else {
      setModalVisible(!modalVisible)
      setIndicator(true)
      fetch('https://contact.herokuapp.com/contact/'+id,{
        method:'put',
        headers:{'content-type': 'application/json'},
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          age: parseInt(age),
          photo: 'N/A'
        })
      })
      .then(function(response) {
        console.log(response.status); // Will show you the status
        if (!response.ok) {
            alert('Failed to update contact');
            setIndicator(false)
            throw new Error("HTTP status " + response.status);
        }
        if(Platform.OS === 'android'){
          ToastAndroid.show('Success update contact', ToastAndroid.SHORT)
        } else {
          alert('Success update contact');
        }
        setIndicator(false)
        resetForm();
        setFormState('create');
        updateContactList();
        return response.status
        // return response.json();
      })
      .catch((err)=>{
        if(Platform.OS === 'android'){
          ToastAndroid.show('Failed to update contact', ToastAndroid.SHORT)
        } else {
          alert('Failed to update contact');
        }
        setIndicator(false)
        console.log(err);
      })
    }
  }

  const addContacts = () => {
    console.log(JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      age: parseInt(age),
      photo: "N/A"
    }));
    // alert(firstName)
    if(firstName.length === 0 || lastName.length === 0 || age.length === 0){
      alert('Please fill all required fields')
    } else {
      setModalVisible(!modalVisible)
      setIndicator(true)
      fetch('https://contact.herokuapp.com/contact', {
        method: 'POST',
        body: JSON.stringify({firstName: firstName,lastName: lastName,age: age,photo: 'N/A'}),
        headers: {
          'Content-type': 'application/json',
        }
      })
      .then(function(response) {
        console.log(response.status); // Will show you the status
        if (!response.ok) {
            throw new Error("HTTP status " + response.status);
        }
        if(Platform.OS === 'android'){
          ToastAndroid.show('Success create contact', ToastAndroid.SHORT)
        } else {
          alert('Success create contact')
        }
        setIndicator(false)
        setID(''); 
        setFirstName(''); 
        setLastName(''); 
        setAge('');
        updateContactList();
        return response.status
      })
      .catch((err)=>{
        if(Platform.OS === 'android'){
          ToastAndroid.show('Failed to create contact', ToastAndroid.SHORT)
        } else {
          alert('Failed to create contact');
        }
        setIndicator(false)
        console.log(err);
      })
    }
  }

  const updateContactList = (isRefresh) => {
    if(isRefresh){
      setRefresh(true)
    }
    fetch('https://contact.herokuapp.com/contact',{
      method:'get'
    })
    .then((res)=>res.json())
    .then(resp=>{
      console.log(true)
      setContacts(resp.data)
      if(isRefresh){
        setRefresh(false)
      }
    })
    .catch((err)=>{
      if(Platform.OS === 'android'){
        ToastAndroid.show('Failed to update contac list', ToastAndroid.SHORT)
      } else {
        alert('Failed to update contact list');
      }
      setIndicator(false)
      console.log(err);
    })
  }

  const resetForm = () => {
      setID(''); 
      setFirstName(''); 
      setLastName(''); 
      setAge('');
  }
  
  const deleteContact = (idDelete) => {
    console.log(idDelete)
    Alert.alert('Confirm Delete', 'Are you sure want to delete this contact?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => {
        setIndicator(true)
        fetch('https://contact.herokuapp.com/contact/'+idDelete,{
          method:'delete',
        })
        .then(function(response) {
          console.log(response.status); // Will show you the status
          if (!response.ok) {
            if(Platform.OS === 'android'){
              ToastAndroid.show('Failed to delete contact', ToastAndroid.SHORT)
            } else {
              alert('Failed to delete contact');
            };
              setIndicator(false)
              // throw new Error("HTTP status " + response.status);
          }
          return response.status
          // return response.json();
        })
        .catch((err)=>{
          if(Platform.OS === 'android'){
            ToastAndroid.show('Failed to delete contact', ToastAndroid.SHORT)
          } else {
            alert('Failed to delete contact');
          }
          setIndicator(false)
          console.log(err);
        })
      }},
    ]);
  } 

  useEffect(() => {
    // Update the document title using the browser API
    fetch('https://contact.herokuapp.com/contact',{
      method:'get'
    })
    .then((res)=>res.json())
    .then(resp=>{
      console.log(true)
      setContacts(resp.data)
    })
    .catch((err)=>{
      console.log(err);
    })
  }, [contacts.length > 0]);

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={[backgroundStyle, {height: Platform.OS === 'android' ? Dimensions.get('window').height + 65 : Dimensions.get('screen').height + 65}]}>
      <StatusBar
        // barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
        {
          indicator || contacts.length === 0
          ?
          <View style={{ position:'absolute', zIndex:99, backgroundColor:'rgba(0,0,0,0.5)', width:'100%', height:'100%', justifyContent:'center' }}>
            <ActivityIndicator size="large" color="#efefef"/>
          </View>
          :
          null
        }
        {/* <Header /> */}
        <View style={{ height:62, justifyContent:'center', backgroundColor:'#b2cbf2', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84, zIndex:1, elevation:5 }}>
          <View style={{ flexDirection:'row', alignItems:'center', alignContent:'center', width:'100%' }}>
            <View style={{ flex:0.25 }}/>
            <View style={{ paddingHorizontal:20, flex:0.75 }}>
              <Text style={{ fontSize:18, fontWeight:'700', textAlign:'center', color:'#fafafa'}}>CRUD Contacts</Text>
            </View>
            <View style={{ alignItems:'center', paddingHorizontal:10, flex:0.15 }}>
              <TouchableOpacity onPress={()=>{ setModalVisible(!modalVisible); resetForm(); }}>
                  <FontAwesomeIcon icon={faPlus} size={21} color='#fafafa'/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        >
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.5)', zIndex:1}}>
            <TouchableOpacity onPress={()=>{ setModalVisible(!modalVisible); setFormState('create'); }} style={{ position:'absolute', right:'5%', top: Platform.OS === 'android' ? '22.5%' : '25%' }}>
              <FontAwesomeIcon icon={faTimes} size={28} color='white' />
            </TouchableOpacity>
            <View
              style={{
                backgroundColor: 'white',
                padding:15,
                width:'90%',
                borderRadius:10,
                zIndex:999
              }}>
                {/* view here */}
                <Text style={{ marginBottom:10, fontWeight:'bold', color:'#2e2e2e', fontSize:16 }}>
                  {
                    formState === 'update'
                    ?
                    `Edit Contact`
                    : formState === 'read'
                    ?
                    `View Contact`
                    :
                    `Add Contact`
                  }
                </Text>
                <Text style={{ marginBottom:5, color:'#2e2e2e', fontWeight:'bold' }}>First Name</Text>
                <TextInput editable={formState!=='read'} onChangeText={setFirstName} value={firstName} placeholder='' style={styles.textInput}/>
                <Text style={{ marginBottom:5, color:'#2e2e2e', fontWeight:'bold' }}>Last Name</Text>
                <TextInput editable={formState!=='read'} onChangeText={setLastName} value={lastName} placeholder='' style={styles.textInput}/>
                <Text style={{ marginBottom:5, color:'#2e2e2e', fontWeight:'bold' }}>Age</Text>
                <TextInput editable={formState!=='read'} onChangeText={setAge} value={age} keyboardType='numeric' placeholder='' style={styles.textInput}/>
                {
                  formState === 'create'
                  ?
                  <TouchableOpacity style={{ backgroundColor:'#b2cbf2', borderRadius:10, marginBottom:10, padding:10, alignItems:'center', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84, }} onPress={()=>{ addContacts(firstName, lastName, age);  }}>
                    <Text style={{ color:'white', fontWeight:'bold' }}>Add to Contact</Text>
                  </TouchableOpacity>
                  : formState === 'update'
                  ?
                  <TouchableOpacity style={{ backgroundColor:'#b2cbf2', borderRadius:10, marginBottom:10, padding:10, alignItems:'center', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84, }} onPress={()=>{ updateContacts(id, firstName, lastName, age); }}>
                    <Text style={{ color:'white', fontWeight:'bold' }}>Edit Contact</Text>
                  </TouchableOpacity>
                  :
                  null
                }
            </View>
          </View>
        </Modal>
        <ScrollView
          // contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={() => updateContactList(true)}/>
          }
          style={{ backgroundColor:'white', padding:5, marginBottom:65, paddingVertical:10 }}
        >
          {
            contacts && contacts.length > 0
            ?
            contacts.map((item, index)=>{
              return(
                <View key={index} style={{ flexDirection:'row', flex:1, marginBottom:5, paddingBottom:5, borderBottomWidth: index !== contacts.length-1 ? 0.5 : null, borderColor:'#ccc' }}>
                  <View style={{ flex:0.2, alignItems:'center' }}>
                    {
                      item.photo === 'N/A'
                      ?
                      <Image source={require('./assets/icon/img_avatar.png')} style={{ width:40, height:40, borderRadius:50 }}/>
                      :
                      <Image source={{ uri: item.photo }} style={{ width:40, height:40, borderRadius:50 }}/>
                    }
                  </View>
                  <View style={{ flex:0.5, paddingLeft:5 }}>
                    <Text style={{ color:'#2e2e2e', fontWeight:'bold', marginBottom:2.5 }}>{item.firstName} {item.lastName}</Text>
                    <Text style={{ color:'#2e2e2e' }}>{item.age} years old</Text>
                  </View>
                  <View style={{ flex:0.5 }}>
                    <View style={{ flexDirection:'row', flex:1, justifyContent:'flex-end' }}>
                      <TouchableOpacity style={{ justifyContent:'center', flex:0.25, alignItems:'center' }} onPress={()=>{ setFirstName(item.firstName); setLastName(item.lastName); setAge(item.age.toString()); setID(item.id); setFormState('read'); setIndexState(index); setModalVisible(!modalVisible);}}>
                        <FontAwesomeIcon icon={faEye}/>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ justifyContent:'center', flex:0.25, alignItems:'center' }} onPress={()=>{ setFirstName(item.firstName); setLastName(item.lastName); setAge(item.age.toString()); setID(item.id); setFormState('update'); setIndexState(index); setModalVisible(!modalVisible);}}>
                        <FontAwesomeIcon icon={faEdit}/>
                      </TouchableOpacity>
                      <TouchableOpacity style={{ justifyContent:'center', flex:0.25, alignItems:'center' }} onPress={()=>{ deleteContact(item.id) }}>
                        <FontAwesomeIcon icon={faTrash}/>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )
            })
            :
            null
          }
          <View style={{ paddingBottom:15 }}/>
        </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  textInput: {
    justifyContent: "center",
    color:'#2e2e2e',
    alignItems: "stretch",
    borderStartWidth : 1,
    borderEndWidth : 1,
    borderTopWidth : 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth : 1,
    height: 36,
    borderRadius:5,
    paddingHorizontal:7.5,
    borderColor: "#9f9f9f",
    backgroundColor: '#ffffff',
    marginBottom:10
  }
});

export default App;
